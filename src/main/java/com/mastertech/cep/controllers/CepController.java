package com.mastertech.cep.controllers;

import com.mastertech.cep.dtos.CepDTO;
import com.mastertech.cep.services.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CepController {

    @Autowired
    private CepService service;

    @GetMapping("/cep/{cep}")
    public CepDTO getCep(@PathVariable String cep) {
        return this.service.getCep(cep);
    }
}
