package com.mastertech.cep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MastertechZipkinExec1CepApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechZipkinExec1CepApplication.class, args);
	}

}
