package com.mastertech.cep.services;

import com.mastertech.cep.dtos.CepDTO;
import com.mastertech.cep.feignclients.CepFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private CepFeignClient client;

    public CepDTO getCep(String cep) {
        return client.getCep(cep);
    }
}
