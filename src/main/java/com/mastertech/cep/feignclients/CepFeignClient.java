package com.mastertech.cep.feignclients;

import com.mastertech.cep.dtos.CepDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "https://viacep.com.br/ws")
public interface CepFeignClient {

    @GetMapping("/{cep}/json/")
    CepDTO getCep(@PathVariable String cep);
}
